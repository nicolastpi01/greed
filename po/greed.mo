��          �   %   �      P  ;   Q  :   �  8   �  8     9   :  9   t  <   �  ;   �  :   '  :   b  <   �  ;   �  ;        R     i     �  	   �     �     �     �     �     �     �  &   	  4   0    e  G   �  8   -  ?   f  E   �  G   �  K   4	  <   �	  2   �	  U   �	  G   F
  E   �
  N   �
  T   #     x  ,   �     �     �     �       "   %     H     V     _  5   z  A   �                                     
                                          	                                               'hjklyubn'. Your location is signified by the '%c' symbol.  'p' to toggle the highlighting of the possible moves, and  'q' to quit.  Command line options to Greed are '-s' to  Other Greed commands are 'Ctrl-L' to redraw the screen,  The object of Greed is to erase as much of the screen as  When you move in a direction, you erase N number of grid  direction.  Your score reflects the total number of squares  eaten.  Greed will not let you make a move that would have  placed you off the grid or over a previously eaten square  possible by moving around in a grid of numbers.  To move,  squares in that direction, N being the first number in that  unless no valid moves exist, in which case your game ends.  use the arrow keys, your number pad, or one of the letters %s - Hit '?' for help. %s: %s: Can't unlink lock.
 %s: ~/%s: Cannot open.
 Bad move. Hit any key.. No high scores. Overriding stale lock... Really quit?  Score:  Usage: %s [-p] [-s]
 Waiting for scorefile access... %d/15
 Welcome to %s, by Matthew Day <mday@iconsys.uu.net>. Project-Id-Version: Greed v4.2
Report-Msgid-Bugs-To: nicolastpi10@gmail.com
POT-Creation-Date: 2018-07-05 16:24-0300
PO-Revision-Date: 2018-07-04 18:03-0300
Last-Translator: nicolas <nicolastpi10@gmail.com>
Language-Team: Argentinian
Language: es_AR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
  'hjklyubn'. La posición del jugador es indicada por el símbolo '%c'.  'p' para resaltar los posibles movimientos en la grilla  'q' para salir. Para acceder a las opciones presiona '-s' para  Otro comando disponible es 'Ctrl-L' para redimensionar la pantalla,  El objetivo de Greed es consumir tantos cuadros de la pantalla como sea Cuando te mueves en una dirección se eliminan N cantidad de cuadros en esa dirección. El puntaje refleja el número total de cuadrados consumidos. Greed no permite movimientos repetidos , posicionarte fuera del tablero, o incluso encima de un cuadro previamente consumido  posible moviéndose a travéz de una grilla de números. Para moverte,  cuadrados en esa dirección, N seria el primer número en ese cuadro  a menos que ya no existan movimientos válidos, en ese caso el juego termina. usa las teclas de direcciones, el teclado númerico, o una de las letras disponibles %s - Presiona '?' por ayuda. %s: %s: no se puede desvincular el bloqueo.
 %s: ~/%s: Imposible abrir.
 Mov. ilegal. Presiona cualquier tecla.. No existe registro de puntajes Anulación del bloqueo obsoleta... desea salir?  Puntos:  Teclas disp: %s [-p] [-s]
 Espere por acceso al archivo de puntuación... %d/15
 Bienvenido a %s, diseñado por Matthew Day <mday@iconsys.uu.net>. 